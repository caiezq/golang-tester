package main

import "github.com/gin-gonic/gin"

func main() {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "2pong145678",
		})

	})
	r.Run(":8089") // listen and serve on 0.0.0.0:8080

}
